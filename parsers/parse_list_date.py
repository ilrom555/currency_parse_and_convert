# #################################################################################### #
# Parser for currency update list date available in in format:
# {"date": ["data.json","data.json"]}
# #################################################################################### #


import os
import json
import re
import datetime


def get_all_file(base_dir, name_dir, ext):
    """
    Search all files which have a given extension within root_path.
    This ignores the case of the extension and searches subdirectories, too.
    """
    all_files = []
    file_dir = os.path.join(base_dir, name_dir)
    date_reg = re.compile(r'((?:[0-2]\d|3[01])\.(?:0\d|1[012])\.(?:\d{4}))')
    for root, dirs, files in os.walk(file_dir):
        for filename in files:
            if filename.lower().endswith(ext):
                date_file = filename[5:-5]
                if re.match(date_reg, date_file):
                    # all_files.append(os.path.join(root, filename))
                    all_files.append(date_file)
    all_files_sorted = sorted(all_files, key=lambda x: datetime.datetime.strptime(x, '%d.%m.%Y'), reverse=True)
    return all_files_sorted


def write_in_file(base_dir, list_file_name):
    # date_list_dict ={}
    save_dir = os.path.join(base_dir, 'data/list_date.json')
    date_list_dict = {"date": list_file_name}
    with open(save_dir, 'w') as outfile:
        json.dump(date_list_dict, outfile)


def main():
    # Отримати директорію де знажодиться скрипт os.path.abspath(__file__)
    # base_dir ="/home/roma/WEB-programing/parser_and_converter_currrencies"
    base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    name_dir = "data"  # назва директорії де знаходяться файли наприклад: "data"
    ext = ".json"      # розширення файлу який будем шукати наприклад: ".json"
    list_file_name = get_all_file(base_dir, name_dir, ext)
    write_in_file(base_dir, list_file_name)


if __name__ == '__main__':
    main()