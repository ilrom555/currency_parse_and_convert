# ######################################################################################## #
# Currency parser from https://westfinance.ub.ua in format:
# {"date": "01.12.2018", "CHF": {"selling_price": "27.70", "purchase_price": "28.70"}, ...}
# ######################################################################################## #
from urllib import request, error
from bs4 import BeautifulSoup
import json
import re


def get_html(url):
    """ Get all html web-page with url """
    try:
        response = request.urlopen(url)
    except error.HTTPError as e:
        print("Помилка: ", e)
    except error.URLError as e:
        print("Помилка: ", e)
    else:
        return response.read()


def get_valid_date(html):
    """ Get valid date exchange rate from web page """
    date_reg = re.compile(r'((?:[0-2]\d|3[01])\.(?:0\d|1[012])\.(?:\d{4}))')
    soup = BeautifulSoup(html, 'lxml')
    row = soup.select_one('div.grid-9 > div.packet-rowset > div.packet-row > div.skip10 > div > div.mt20')
    date = re.findall(date_reg, row.text)
    date_dict = {'date': date[0]}
    return date_dict


def get_exchange_rate(html):
    """ Get valid value exchange rate """
    curr_dict = dict()
    currency_name_reg, price_reg = re.compile(r'([A-Z][A-Z][A-Z])'), re.compile(r'(\d+\.?\d*)')
    soup = BeautifulSoup(html, 'lxml')
    rows = soup.select('div.grid-9 > div.packet-rowset > div.packet-row > div.skip10 > div table tr')
    for row in rows:
        currency_name = re.findall(currency_name_reg, row.text)
        price = re.findall(price_reg, row.text)
        if currency_name and len(currency_name) == 1 and price and len(price) == 2:
            curr_dict.update({currency_name[0]: {'selling_price': price[0], 'purchase_price': price[1]}})
    curr_dict.update({"UAH": {"selling_price": "1.00", "purchase_price": "1.00"}})
    return curr_dict
    #     if currency_name and len(currency_name) == 1 and price and len(price) == 2:
    #         curr_arr += [{'currency_name': currency_name[0], 'selling_price': price[0], 'purchase_price': price[1]}]
    # return curr_arr


def write_in_file(date_dict, curr_dict):
    date_dict.update(curr_dict)
    date = date_dict["date"]
    with open('data/data-{}.json'.format(date), 'w') as outfile:
        json.dump(date_dict, outfile)


def main():
    url = "https://westfinance.ub.ua"
    html = get_html(url)
    date_dict = get_valid_date(html)
    curr_dict = get_exchange_rate(html)
    write_in_file(date_dict, curr_dict)


if __name__ == '__main__':
    main()
