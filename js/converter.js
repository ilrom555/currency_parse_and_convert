// let enableDates = [];
function loadListDate() {
    let datepicker = document.getElementById("datepicker");
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function(){
        if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
                let data =JSON.parse(this.responseText);
                let list_date = data["date"];
                let current_date = list_date[0];
                datepicker.value = current_date;
                loadCurrencies(current_date);
                // for(let index in list_date){
                //     enableDates[index]=list_date[index];
                // }
        }
    };
    xhr.open("GET", 'data/list_date.json', true);
    xhr.send();
}


function loadCurrencies(current_date) {
    let from = document.getElementById("from");
    let to = document.getElementById("to");

    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function(){
        if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
                let data =JSON.parse(this.responseText);
                let options="";
                let date_value="--.--.--";
                for(let key_cur in data){
                    if(key_cur==='date'){
                        date_value = data[key_cur];
                    }else {
                        options = options + '<option>' + key_cur + '</option>';
                    }
                }
                from.innerHTML = options;
                to.innerHTML = options;
        }

    };
    xhr.open("GET", 'data/data-'+current_date+'.json', true);
    xhr.send();
}

function convertCurrency() {
    let datepicker = document.getElementById("datepicker").value;
    let from = document.getElementById("from").value;
    let to = document.getElementById("to").value;
    let amount = document.getElementById("amount").value;
    let result = document.getElementById("result");
    let status;
    if (document.getElementById('r1').checked) {
      status = document.getElementById('r1').value;
    }else if(document.getElementById('r2').checked){
      status = document.getElementById('r2').value;
    }
    if(from.length>0 && to.length>0 && amount.length>0 && status.length>0){
        let xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
                let data = JSON.parse(this.responseText);
                let fact = (parseFloat(amount) * parseFloat(data[from][status]))/parseFloat(data[to][status]);
                if(fact){
                    result.innerHTML = fact.toFixed(2);
                }
            }
        };
        xhr.open("GET", 'data/data-'+datepicker+'.json', true);
        xhr.send();
    }
}

function changeValues(){
    let temp = document.getElementById('from').selectedIndex;
    document.getElementById('from').selectedIndex = document.getElementById('to').selectedIndex;
    document.getElementById('to').selectedIndex = temp;
    convertCurrency();
}
