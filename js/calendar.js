function loadJSON(path, success, error)
{
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function()
        {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                if (xhr.status === 200) {
                    if (success)
                        success(JSON.parse(xhr.responseText));
                } else {
                    if (error)
                        error(xhr);
                }
            }
        };
    xhr.open("GET", path, true);
    xhr.send();
}

let a = loadJSON('data/list_date.json',
    function(data) {
        let enableDates=data.date;

        function isAvailable(date) {
            let dt ="";
            if (date.getDate() < 10) {
                dt = '0' + date.getDate();
            }else{
                dt = date.getDate();
            }
            if ((date.getMonth()+1) < 10) {
                dt += "." + "0" + (date.getMonth()+1);
            }else{
                dt += "." + (date.getMonth()+1);
            }
            dt += "." + date.getFullYear();
            if (enableDates.indexOf(dt) != -1) {
                return [true,"",""];
            } else {
                return [false,"",""];
            }
        }

        $('#datepicker').datetimepicker({
                beforeShowDay: isAvailable,
        });
    },
    function(xhr){
        console.error(xhr);
    }
);


$.datetimepicker.setLocale('uk');

$('#datepicker').datetimepicker({
    showOn: "button",
    buttonImage: "http://jqueryui.com/resources/demos/datepicker/images/calendar.gif",
    buttonImageOnly: true,
    clickInput: true,
    // showOn: "button",
    // buttonImage: "img/calendar.png",
    i18n: {
        uk: {
            months: [
                'Cічень', 'Лютий', 'Березень', 'Квітень',
                'Травень', 'Червень', 'Липень', 'Серпень',
                'Вересень', 'Жовтень', 'Листопад', 'Грудень',
            ],
            dayOfWeek: [
                "Нд.", "Пн.", "Вт.", "Ср.",
                "Чт.", "Пт.", "Сб.",
            ]
        }
    },
    closeText: 'Закрити',
    prevText: '<Попер',
    nextText: 'Наст>',
    currentText: 'Сьогодні',
    timepicker: false,
    format: 'd.m.Y'
});




